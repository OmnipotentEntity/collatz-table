use rug::{Complete, Integer};
use std::io::Write;
use std::ops::Range;

const TABLE_EXPONENT: u32 = 18;

struct BigIntRange(Range<Integer>);

impl Iterator for BigIntRange {
    type Item = Integer;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.start += 1u32;
        if self.0.start == self.0.end {
            None
        } else {
            Some(self.0.start.clone())
        }
    }
}

fn precompute_tables(table_exponent: u32) -> (Vec<u32>, Vec<u32>, Vec<u32>, Vec<u32>) {
    assert!(table_exponent > 1 && table_exponent <= 20);
    let table_size = 2u32.pow(table_exponent - 1);
    let power_of_two = 2u32.pow(table_exponent);

    let mut mult_const: Vec<u32> = vec![];
    let mut add_const: Vec<u32> = vec![];
    let mut iter_const: Vec<u32> = vec![];
    let mut short_iter: Vec<u32> = vec![];

    mult_const.reserve_exact(table_size as usize);
    add_const.reserve_exact(table_size as usize);
    iter_const.reserve_exact(table_size as usize);
    short_iter.reserve_exact(table_size as usize);

    for n in 0..table_size {
        let mut add_coeff = 2*n + 1;
        let mut mult_coeff = power_of_two;
        let mut iters = 0;

        while mult_coeff % 2 == 0 {
            if add_coeff % 2 == 0 {
                mult_coeff /= 2;
                add_coeff /= 2;
            } else {
                mult_coeff = 3 * mult_coeff / 2;
                add_coeff = (3*add_coeff + 1)/2;
                iters += 1;
            }
        }

        mult_const.push(mult_coeff);
        add_const.push(add_coeff);
        iter_const.push(iters);
    }

    for n in 0..table_size {
        let mut n = 2*n + 1;
        let mut iters = 0;

        while n != 1 {
            if n % 2 == 0 {
                n /= 2;
            } else {
                n = 3*n + 1;
                n /= 2;
                iters += 1;
            }
        }

        short_iter.push(iters);
    }

    (mult_const, add_const, iter_const, short_iter)
}

fn main() {
    let residue_size = 2u32.pow(TABLE_EXPONENT);

    let (mult_const, add_const, iter_const, short_iter) = precompute_tables(TABLE_EXPONENT);

    // A relatively small power of 3, which is unusually close to a power of 2.
    // 2**301994, 3**190537, diff ratio = 6.450750485236458e-08

    let center_range: Integer =
        (Integer::u_pow_u(2, 301994).complete() * Integer::u_pow_u(3, 190537).complete()).sqrt();
    let range_width: usize = 50;

    let iter_range =
        BigIntRange(center_range.clone() - range_width..center_range + range_width + 1u32);

    let iter_counts: Vec<u64> = iter_range
        .into_iter()
        .map(|i: Integer| {
            let mut input = i.clone();
            let mut lowest_seen = i;
            let mut iterations: u64 = 0;
            let mut checkpoint: u64 = 300000;
            let cp_freq: u64 = 300000;
            println!("...{}", input.mod_u(10000000u32));

            // First, give us an odd number, shift by ctz.
            input >>= input.find_one(0).unwrap();

            while input > residue_size {
                if iterations > checkpoint {
                    print!("iters: {}\r", iterations);
                    std::io::stdout().flush().expect("Can't flush");
                    checkpoint += cp_freq;
                }

                let res_idx = input.mod_u(residue_size) as usize >> 1;
                input = mult_const[res_idx] * (input >> TABLE_EXPONENT) + add_const[res_idx];
                input >>= input.find_one(0).unwrap();
                iterations += iter_const[res_idx] as u64;

                if input == lowest_seen {
                    // We've detected a loop!
                    println!("DETECTED A LOOP: {}", input);
                    std::io::stdout().flush().expect("Can't flush");
                    panic!("DETECTED A LOOP!!!!!!");
                }

                if input < lowest_seen {
                    lowest_seen = input.clone();
                }
            }

            iterations += short_iter[(input.to_usize().unwrap() - 1) / 2] as u64;
            println!("");

            iterations
        })
        .collect();

    for (i, count) in iter_counts.iter().enumerate() {
        let offset: isize = i as isize - range_width as isize;
        println!("{}: {}", offset, count);
    }
}
